extends Node

var last_chat_npc = null

func _ready():
	$Player.connect("chat_to_npc", self, "_on_chat_with_npc")
	$CanvasLayer/ChatBox.connect("chat_finished", self, "_on_chat_finished")
	$Timer.connect("timeout", self, "_on_timer")

func _on_chat_with_npc(npc):
	$CanvasLayer/ChatBox.show_chat(npc.chat_entries)
	$Player.disable_input = true
	npc.on_chat($Player.position)
	last_chat_npc = npc

func _on_chat_finished():
	$Player.disable_input = false
	if last_chat_npc != null:
		last_chat_npc.on_chat_finished()

func _on_timer():
	$Player.warp_in()
	$CanvasLayer/ScreenFadeIn.fade_in()