extends Container

signal chat_finished

var disabled = true

onready var conversation = $ColorRect/Container/ColorRect/Container/ColorRect/Container/MrProgConversation

func _ready():
	$AnimationPlayer.connect("animation_finished", self, "_on_animation_finished")
	conversation.connect_all_text_visible(self, "_on_all_text_visible")

func show_chat(chat_entries):
	show()
	$AnimationPlayer.play("slide_in")
	conversation.on_shown(chat_entries)
	$NextArrow.hide()

func _process(delta):
	if disabled:
		return

	if Input.is_action_just_pressed("interact"):
		_handle_interact()

func _handle_interact():
	if !conversation.is_all_text_visible():
		conversation.set_all_text_visible()
		return

	if not _next_entry() and $AnimationPlayer.assigned_animation != "slide_out":
		$AnimationPlayer.play("slide_out")


func _next_entry():
	$NextArrow.hide()
	return conversation.next_entry()

func _on_animation_finished(anim_name):
	if anim_name == "slide_in":
		_on_slide_in_completed()
	elif anim_name == "slide_out":
		_on_slide_out_completed()

func _on_slide_in_completed():
	disabled = false

func _on_slide_out_completed():
	hide()
	disabled = true
	emit_signal("chat_finished")

func _on_all_text_visible():
	$NextArrow.show()
