extends KinematicBody2D

const NPC = preload("res://NPCs/NPC.gd")
signal chat_to_npc(npc)

const TIME_BETWEEN_CHANGES = 1.0
const DIRECTIONS = [ "down", "ldown", "left", "lup", "up", "rup", "right", "rdown" ]
const MOVEMENT_SPEED = 60
const MOVE_INPUT_PROCESSING_DELAY = 0.1
const FACING_DISTANCE = 2

var megaman_renderer

var movement_direction = Vector2(0, 0)
var last_move_input_delta = 0
var disable_input = true
var facing_direction = Vector2(0, 1)

func _ready():
	megaman_renderer = $IsoYToZIndex/MegamanRenderer
	megaman_renderer.get_node("AnimationPlayer").connect("animation_finished", self, "_on_animation_finished")

func warp_in():
	megaman_renderer.set_animation("warp_in")

func _calculate_movement_direction_from_input():
	var movement_direction = Vector2(0, 0)
	if Input.is_action_pressed("move_left"):
		movement_direction.x -= 1
	if Input.is_action_pressed("move_right"):
		movement_direction.x += 1
	if Input.is_action_pressed("move_down"):
		movement_direction.y += 1
	if Input.is_action_pressed("move_up"):
		movement_direction.y -= 1

	if movement_direction.x != 0 and movement_direction.y != 0:
		movement_direction.y /= 2

	return movement_direction.normalized()

func _on_animation_finished(name):
	if name == "warp_in":
		disable_input = false

func _handle_input(delta):
	if disable_input:
		if not _is_zero(movement_direction):
			movement_direction = Vector2(0, 0)
			megaman_renderer.handle_movement(movement_direction)
		return

	last_move_input_delta += delta
	if last_move_input_delta > MOVE_INPUT_PROCESSING_DELAY:
		last_move_input_delta -= MOVE_INPUT_PROCESSING_DELAY
		movement_direction = _calculate_movement_direction_from_input()
		megaman_renderer.handle_movement(movement_direction)

	if Input.is_action_just_pressed("interact"):
		var item_we_face = _get_item_we_are_facing()
		if item_we_face != null and item_we_face is NPC:
				emit_signal("chat_to_npc", item_we_face)

func _physics_process(delta):
	_handle_input(delta)

	if not _is_zero(movement_direction):
		_move(movement_direction, delta)
		facing_direction = movement_direction

func _move(movement_vec2, delta):
	var movement_vec2_with_speed = movement_vec2 * MOVEMENT_SPEED
	var collision = move_and_collide(movement_vec2_with_speed * delta)
	if collision == null:
		return

	position -= collision.travel
	if not _is_near_zero(movement_vec2 + _calc_isometric_normal(collision.normal)):
		var slideVec2 = movement_vec2_with_speed.slide(collision.normal).normalized() * MOVEMENT_SPEED
		move_and_collide(slideVec2 * delta)


# The standard 2D collision normal is not the correct perspective.
# This method fixes it by halving the x and doubling the y.
func _calc_isometric_normal(vec2):
	return vec2 * Vector2(2, 0.5)

func _is_near_zero(vec2):
	return abs(vec2.x) < 0.05 and abs(vec2.y) < 0.05

func _is_zero(vec2):
	return vec2.x == 0.0 and vec2.y == 0

func _get_item_we_are_facing():
	var facing_move = facing_direction * FACING_DISTANCE
	var collision = move_and_collide(facing_move)
	if collision == null:
		position -= facing_move
		return

	position -= collision.travel
	return collision.collider
