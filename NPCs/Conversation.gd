extends HBoxContainer

func _ready():
	var mugshot = $mugshot
	$ChatTextPlayer.connect("word_started", mugshot, "on_word_started")
	$ChatTextPlayer.connect("all_text_visible", mugshot, "on_all_text_visible")
	$ChatTextPlayer.connect("played_non_word_char", mugshot, "on_non_word_character_played")

func on_shown(chat_entries):
	$ChatTextPlayer.on_shown(chat_entries)

func next_entry():
	return $ChatTextPlayer.next_entry()

func is_all_text_visible():
	return $ChatTextPlayer.is_all_text_visible()

func set_all_text_visible():
	$ChatTextPlayer.set_all_text_visible()

func connect_all_text_visible(target, method):
	$ChatTextPlayer.connect("all_text_visible", target, method)
