extends Node2D

func _process(delta):
	var zVal = global_position.y + get_tree().get_root().canvas_transform.get_origin().y
	z_index = _clampi(zVal, -4095, 4096)

func _clampi(val, minVal, maxVal):
	if val > maxVal:
		return maxVal
	if val < minVal:
		return minVal
	return val
