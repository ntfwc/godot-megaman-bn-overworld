extends ColorRect

func _ready():
	$AnimationPlayer.connect("animation_finished", self, "_on_animation_finished")

func fade_in():
	$AnimationPlayer.play("fade_in")

func _on_animation_finished(anim_name):
	hide()