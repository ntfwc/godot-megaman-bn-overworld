extends Node2D

var animation_player

func _ready():
	animation_player = $AnimationPlayer
	$Sprite.set_texture(null)

func set_animation(name):
	if get_animation() != name:
		animation_player.play(name)

func handle_movement(direction_vec2):
	if direction_vec2.x == 0 and direction_vec2.y == 0:
		_set_stop_animation()
	else:
		_set_run_animation(direction_vec2)

func _set_run_animation(direction_vec2):
	var animation = _get_run_animation(direction_vec2)
	if animation != null:
		set_animation(animation)

func _set_stop_animation():
	var current_animation = get_animation()
	if current_animation.begins_with("run_"):
		set_animation(current_animation.substr(4, current_animation.length() - 4))

func _get_run_animation(direction_vec2):
	if direction_vec2.x > 0:
		if direction_vec2.y < 0:
			return "run_rup"
		elif direction_vec2.y > 0:
			return "run_rdown"
		return "run_right"
	elif direction_vec2.x < 0:
		if direction_vec2.y < 0:
			return "run_lup"
		elif direction_vec2.y > 0:
			return "run_ldown"
		return "run_left"

	if direction_vec2.y < 0:
		return "run_up"
	elif direction_vec2.y > 0:
		return "run_down"

	return null

func get_animation():
	return animation_player.assigned_animation
