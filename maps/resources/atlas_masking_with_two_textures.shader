shader_type canvas_item;

uniform sampler2D mask_texture;
uniform ivec2 atlas_position;

uniform ivec2 background_atlas_position;
uniform sampler2D background_texture;

ivec2 computePixelPosition(vec2 UV, ivec2 texture_size)
{
	return ivec2(vec2(texture_size) * UV);
}

vec2 computeUVCoordinates(ivec2 pixel_position, ivec2 texture_size)
{
	return (vec2(pixel_position) + vec2(0.5, 0.5))/vec2(texture_size);
}

bool isMasked(ivec2 pixel_position)
{
	ivec2 mask_size = textureSize(mask_texture, 0);
	ivec2 mask_pixel_position = pixel_position - atlas_position;
	return texture(mask_texture, computeUVCoordinates(mask_pixel_position, mask_size)).r != 1.0;
}

vec4 chooseFromTextures(sampler2D TEXTURE, ivec2 pixel_position, ivec2 texture_size)
{
	vec4 color = texture(TEXTURE, computeUVCoordinates(pixel_position, texture_size));
	if (color.a == 0.0)
	{
		ivec2 background_pixel_position = pixel_position - atlas_position + background_atlas_position;
		color = texture(background_texture, computeUVCoordinates(background_pixel_position, textureSize(background_texture, 0)));
	}
	return color;
}

void fragment()
{
	ivec2 texture_size = textureSize(TEXTURE, 0);
	ivec2 pixel_position = computePixelPosition(UV, texture_size);
	if (isMasked(pixel_position))
		discard;
	else
		COLOR = chooseFromTextures(TEXTURE, pixel_position, texture_size);
}
