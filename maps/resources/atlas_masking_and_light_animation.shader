shader_type canvas_item;

uniform sampler2D mask_texture;
uniform ivec2 atlas_position;
uniform ivec2 mask_offset = ivec2(0, 0);

uniform int animation_offset = -3;

uniform vec4 light_color_match1 : hint_color = vec4( 0.972549, 0.972549, 0, 1 );
uniform vec4 light_color_match2 : hint_color = vec4( 0.752941, 0.752941, 0.12549, 1 );
uniform vec4 light_color_match3 : hint_color = vec4( 0.564706, 0.564706, 0.25098, 1 );
uniform vec4 light_color_match4 : hint_color = vec4( 0.376471, 0.376471, 0.376471, 1 );

uniform vec4 light_color : hint_color = vec4( 0.972549, 0.972549, 0, 1 );
uniform vec4 light_color_off : hint_color = vec4( 0.376471, 0.376471, 0.376471, 1 );

ivec2 computePixelPosition(vec2 UV, ivec2 texture_size)
{
	return ivec2(vec2(texture_size) * UV);
}

vec2 computeUVCoordinates(ivec2 pixel_position, ivec2 texture_size)
{
	return (vec2(pixel_position) + vec2(0.5, 0.5))/vec2(texture_size);
}

bool isMasked(ivec2 pixel_position)
{
	ivec2 mask_size = textureSize(mask_texture, 0);
	ivec2 mask_pixel_position = pixel_position - atlas_position + mask_offset;
	return texture(mask_texture, computeUVCoordinates(mask_pixel_position, mask_size)).r != 1.0;
}

bool matches(vec4 color, vec4 match_color)
{
	float MATCH_RANGE = 0.001;
	return all(lessThan(abs(color - match_color), vec4(MATCH_RANGE)));
}

vec4 determineColor(sampler2D TEXTURE, vec2 UV, int pixel_x_offset)
{
	vec4 color = texture(TEXTURE, UV);
	if (matches(color, light_color_match1) || matches(color, light_color_match2)
	    || matches(color, light_color_match3) || matches(color, light_color_match4))
	{
		float mult = float(((pixel_x_offset / 2) + animation_offset ) % 4) / 3.0;
		return ((light_color - light_color_off) * mult) + light_color_off;
	}
	
	return color;
}

void fragment()
{
	ivec2 texture_size = textureSize(TEXTURE, 0);
	ivec2 pixel_position = computePixelPosition(UV, texture_size);
	if (isMasked(pixel_position))
		discard;
	else
		COLOR = determineColor(TEXTURE, computeUVCoordinates(pixel_position, texture_size), pixel_position.x);
}
