extends Node2D

func show():
	.show()
	$AnimationPlayer.play("idle")

func hide():
	.hide()
	$AnimationPlayer.stop()
