shader_type canvas_item;

uniform vec4 keyed_color : hint_color;

void fragment() {
	float MATCH_RANGE = 0.001;
	
	vec4 tex_color = texture(TEXTURE, UV);
	if (all(lessThan(abs(tex_color - keyed_color), vec4(MATCH_RANGE))))
		discard;
	else
		COLOR = tex_color;
}
