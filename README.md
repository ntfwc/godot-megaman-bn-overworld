# Description #

This is a little project I worked on as part of learning Godot. This project re-creates some of the overworld mechanics from the Mega Man Battle Network series.

It's not complete, but it could be a good starting point or reference for creating for similar games.

# IMPORTANT: Resources #

This project used some ripped resources from the original games. These resources are not distributed here, as they are proprietary. You need to first download or replace the resources. They are described in REQUIRED_RESOURCES.txt.

The easy way to get the resources is to run the script at [https://gitlab.com/ntfwc/godot-megaman-bn-overworld-resource-script](https://gitlab.com/ntfwc/godot-megaman-bn-overworld-resource-script) in the directory.

# Using it #

This is a Godot 3 project. Refer to the Godot 3 documentation for how to use the editor and launch a project.

# Controls #

- W: up
- A: left
- S: down
- D: right
- E: interact
