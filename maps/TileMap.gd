extends TileMap

const LIGHT_ANIMATED_TILES = [2, 3, 4, 5, 6]

func _ready():
	$AnimationPlayer.play("light_animation")

func set_light_animation_offset(offset):
	_set_light_animation_param("animation_offset", offset)

func set_light_color(color):
	_set_light_animation_param("light_color", color)

func _set_light_animation_param(param_name, value):
	for index in LIGHT_ANIMATED_TILES:
		tile_set.tile_get_material(index).set_shader_param(param_name, value)