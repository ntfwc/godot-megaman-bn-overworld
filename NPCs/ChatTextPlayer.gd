extends Container

signal all_text_visible
signal word_started
signal played_non_word_char

const CHARACTER_DELAY = 0.05
const NON_WORD_CHARS = " .,';\"?"

var time_since_last_character = 0
var visible_text_position = 0

var chat_entries
var index = 0

var shown = false

func on_shown(chat_entries):
	shown = true
	self.chat_entries = chat_entries
	index = -1
	next_entry()

func next_entry():
	index += 1
	if index >= chat_entries.size():
		return false

	$Label.text = chat_entries[index]
	_set_visible_text_position(1)
	time_since_last_character = 0
	if _is_starting_word():
		_on_starting_word()
	if is_all_text_visible():
		_on_all_text_visible()
	return true

func _count_visible_characters(text):
	var count = 0
	for c in text:
		if c != ' ':
			count += 1
	return count

func _process(delta):
	if not shown or is_all_text_visible():
		return

	time_since_last_character += delta
	if time_since_last_character >= CHARACTER_DELAY:
		time_since_last_character -= CHARACTER_DELAY
		_set_visible_text_position(visible_text_position + 1)
		if _is_starting_word():
			_on_starting_word()
		if is_all_text_visible():
			_on_all_text_visible()

func set_all_text_visible():
	_set_visible_text_position($Label.text.length() - 1)
	_on_all_text_visible()

func is_all_text_visible():
	return visible_text_position >= $Label.text.length() - 1

func _on_all_text_visible():
	emit_signal("all_text_visible")

func _on_starting_word():
	emit_signal("word_started")

func _is_starting_word():
	if visible_text_position == 0:
		return false

	if visible_text_position == 1 && _is_word_char($Label.text[0]):
		return true

	return !_is_word_char($Label.text[visible_text_position - 1]) and _is_word_char($Label.text[visible_text_position])

func _get_visible_text_position():
	var actual_position = -1
	var visible_position = 0
	for c in $Label.text:
		actual_position += 1
		if c != ' ':
			visible_position += 1
			if (visible_position >= $Label.visible_characters):
				break
	return actual_position

func _is_word_char(character):
	return NON_WORD_CHARS.find(character) == -1

func _set_visible_text_position(value):
	visible_text_position = value
	$Label.visible_characters = _determine_visible_characters(visible_text_position)
	if !_is_word_char($Label.text[visible_text_position]):
		emit_signal("played_non_word_char")

func _determine_visible_characters(text_position):
	if text_position >= $Label.text.length() - 1:
		return -1

	var visible_characters = 0
	for i in range(text_position):
		if $Label.text[i] != ' ':
			visible_characters += 1
	return visible_characters