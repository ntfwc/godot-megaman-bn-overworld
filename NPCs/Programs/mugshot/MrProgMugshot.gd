extends TextureRect

func _ready():
	$AnimationPlayer.play("idle")
	$AnimationPlayer.connect("animation_finished", self, "_on_animation_finished")

func on_all_text_visible():
	_set_idle_animation()

func on_word_started():
	$AnimationPlayer.play("start_word")

func on_non_word_character_played():
	_set_idle_animation()

func _on_animation_finished(anim_name):
	if anim_name == "start_word":
		$AnimationPlayer.play("saying_word")

func _set_idle_animation():
	if $AnimationPlayer.assigned_animation != "idle":
		$AnimationPlayer.play("idle")