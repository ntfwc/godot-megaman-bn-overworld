shader_type canvas_item;

uniform vec4 from_color1 : hint_color;
uniform vec4 to_color1 : hint_color;
uniform vec4 from_color2 : hint_color;
uniform vec4 to_color2 : hint_color;
uniform vec4 from_color3 : hint_color;
uniform vec4 to_color3 : hint_color;
uniform vec4 from_color4 : hint_color;
uniform vec4 to_color4 : hint_color;
uniform vec4 from_color5 : hint_color;
uniform vec4 to_color5 : hint_color;
uniform vec4 from_color6 : hint_color;
uniform vec4 to_color6 : hint_color;
uniform vec4 from_color7 : hint_color;
uniform vec4 to_color7 : hint_color;
uniform vec4 from_color8 : hint_color;
uniform vec4 to_color8 : hint_color;
uniform vec4 from_color9 : hint_color;
uniform vec4 to_color9 : hint_color;

bool matches(vec4 color, vec4 match_color)
{
	float MATCH_RANGE = 0.001;
	return all(lessThan(abs(color - match_color), vec4(MATCH_RANGE)));
}

vec4 determineColor(vec4 texture_color)
{
	if (matches(texture_color, from_color1))
		return to_color1;
	if (matches(texture_color, from_color2))
		return to_color2;
	if (matches(texture_color, from_color3))
		return to_color3;
	if (matches(texture_color, from_color4))
		return to_color4;
	if (matches(texture_color, from_color5))
		return to_color5;
	if (matches(texture_color, from_color6))
		return to_color6;
	if (matches(texture_color, from_color7))
		return to_color7;
	if (matches(texture_color, from_color8))
		return to_color8;
	if (matches(texture_color, from_color9))
		return to_color9;
	return texture_color;
}

void fragment()
{
	COLOR = determineColor(texture(TEXTURE, UV));
}
