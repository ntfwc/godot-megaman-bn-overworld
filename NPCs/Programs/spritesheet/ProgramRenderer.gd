tool
extends Node2D

func set_direction(left, down):
	if down:
		$Body.texture = preload("res://NPCs/Programs/spritesheet/down_right.tres")
	else:
		$Body.texture = preload("res://NPCs/Programs/spritesheet/up_right.tres")

	$Body.flip_h = left