tool
extends "../NPC.gd"

export (bool) var initial_facing_left = true setget _set_initial_facing_left
export (bool) var initial_facing_down = true setget _set_initial_facing_down

onready var program_renderer = $IsoYToZIndex/ProgramRenderer

func _ready():
	_reset_facing_direction()

func on_chat(player_position):
	var position_diff = global_position - player_position
	var facing_left = position_diff.x > 0
	var facing_down = position_diff.y < 0
	$IsoYToZIndex/ProgramRenderer.set_direction(facing_left, facing_down)

func on_chat_finished():
	_reset_facing_direction()

func _reset_facing_direction():
	if program_renderer == null:
		return
	program_renderer.set_direction(initial_facing_left, initial_facing_down)

func _set_initial_facing_left(value):
	initial_facing_left = value
	_reset_facing_direction()

func _set_initial_facing_down(value):
	initial_facing_down = value
	_reset_facing_direction()