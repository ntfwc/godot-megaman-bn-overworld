shader_type canvas_item;

vec2 calculateUv(vec2 UV, float TIME)
{
	float TIME_BETWEEN_FRAMES = 0.2666;
	
	float TIME_TO_OFFSET_ONE_FRAME_X = 2.1333;
	float X_OFFSET_DIVISOR = TIME_TO_OFFSET_ONE_FRAME_X * 8.0;
	
	float frameU = mod(UV.x - TIME/X_OFFSET_DIVISOR, 1.0/8.0);
	float frame_offsetU = floor(mod(-TIME / TIME_BETWEEN_FRAMES, 8.0));
	
	float TIME_TO_OFFSET_ONE_FRAME_Y = TIME_TO_OFFSET_ONE_FRAME_X * 2.0;
	
	return vec2(frameU + frame_offsetU * 1.0/8.0, UV.y - TIME/TIME_TO_OFFSET_ONE_FRAME_Y);
}

void fragment()
{
	COLOR = texture(TEXTURE, calculateUv(UV, TIME));
}